export interface IDialogSerivce {
  open(config: IDialogConfiguration): Promise<IDialog>;
}

export interface IDialogConfiguration {
  content: string;
  title: string;
  buttons: string[];
}

export interface IDialog {
  close(action: string)
}
